import random

class CardDeck:
    @staticmethod
    def get_full_deck():
        """All cards in a deck"""

        return ['TS', 'QC', '8S', '8D', 'QH', '2D', '3H', 'KH', '9H', '2H','TH', 'KS', 'KC',
                '9D', 'JH', '7H', 'JD', '2S', 'QS', 'TD', '2C', '4H', '5H', 'AD', '4D', '5D',
                '6D', '4S', '9S', '5S', '7S', 'JS', '8H', '3D', '8C', '3S', '4C', '6S', '9C',
                'AS', '7C', 'AH', '6H', 'KD', 'JC', '7D', 'AC', '5C', 'TC', 'QD', '6C', '3C']

    @staticmethod
    def read_cards_from_file(file):
        """Returns a list of card deck arrays. First element of the array is the bottom card."""
        try:
            openedFile = open(file, "r")
            lastLine = openedFile.readline()
            lastLine = lastLine.strip()
            cardDeck = list()
            cardDecks = list()
            cardsInADeck = 0
            while lastLine != '' and lastLine != '#':
                lineElements = lastLine.split(' ')
                if(len(lineElements) != 13):
                    print("Bad input format")
                    return None
                cardsInADeck += len(lineElements)
                cardDeck.extend(lineElements)
                if cardsInADeck == 52:
                    if(CardDeck.validate_card_deck(cardDeck)):
                        cardDecks.append(cardDeck.copy())
                    else:
                        print("Bad card format")
                    cardDeck.clear()
                    cardsInADeck = 0

                lastLine = openedFile.readline()
                lastLine = lastLine.strip()
            openedFile.close()
            if cardDecks == None or len(cardDecks) == 0:
                return None
            else:
                return cardDecks
        except Exception as e:
            print(type(e), '::', e)
            return None
    
    @staticmethod
    def validate_card_deck(cardDeck):
        """Checks if all cards are correct."""
        return all(card in CardDeck.get_full_deck() for card in cardDeck)

    @staticmethod
    def generate_card_deck():
        """Generates a random card deck."""
        cardDeck = CardDeck.get_full_deck()
        random.shuffle(cardDeck)
        return cardDeck
    
    @staticmethod
    def generate_card_deck_to_file(fileName):
        """Generates a random card deck. And writes it to a file."""
        try:
            cardDeck = CardDeck.generate_card_deck()
            openedFile = open(fileName, "w")
            i = 0
            for card in cardDeck:
                i += 1
                openedFile.write(card + " ")
                if(i == 13):
                    i = 0
                    openedFile.write('\n')
            openedFile.write('#')
            openedFile.close()
        except Exception as e:
            print(type(e), '::', e)
            return None