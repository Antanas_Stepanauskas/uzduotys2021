class CardCircleGame:

    def __init__(self, cardDeck):
        self.__cardDeck = cardDeck
        self.__cardPiles = []

    def set_card_deck(self, cardDeck):
        """Sets new cards in a deck"""
        self.__cardDeck = cardDeck
        self.__cardPiles.clear()

    def _fill_piles(self):
        """Fills 13 piles of cards"""
        for i in range(13):
            self.__cardPiles.append(self.Pile())

        i = 0
        for j in range(len(self.__cardDeck) - 1,-1,-1):
            self.__cardPiles[i].add_face_down(self.__cardDeck[j])
            i += 1
            if(i == 13):
                i = 0

    def calculate_answer(self):
        """Calculates the amount of moves and the last picked card in a game of Patience Clock"""
        self._fill_piles()
        moveCount = 0

        currentCard = self.__cardPiles[12].pop_face_down()
        lastCard = currentCard

        while(True):
            curIndex = self._get_index(currentCard)
            self.__cardPiles[curIndex].add_face_up(currentCard)
            lastCard = currentCard
            currentCard = self.__cardPiles[curIndex].pop_face_down()
            moveCount += 1
            if(currentCard == None):
                break

        return format(moveCount, '02d') + ', ' + lastCard

    def _get_index(self, card):
        cardsWithPosition = ['A','2','3','4','5','6','7','8','9','T','J','Q','K']
        return cardsWithPosition.index(card[0])
        
    class Pile:
        def __init__(self):
            self.__fdCount = 0
            self.__fuCount = 0
            self.__fdStack = []
            self.__fuStack = []

        def add_face_down(self, value):
            self.__fdCount += 1
            self.__fdStack.append(value)

        def add_face_up(self, value):
            self.__fuCount += 1
            self.__fuStack.append(value)
        
        def get_face_down_count(self):
            return self.__fdCount

        def get_face_up_count(self):
            return self.__fuCount
        
        def pop_face_down(self):
            if(self.__fdCount > 0):
                self.__fdCount -= 1
                return self.__fdStack.pop()
            else:
                return None

        def pop_face_up(self):
            if(self.__fuCount > 0):
                self.__fuCount -= 1
                return self.__fuStack.pop()
            else:
                return None