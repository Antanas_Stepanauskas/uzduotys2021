import os
from CardDeck import CardDeck as cd
from CardCircleGame import CardCircleGame as cdg

def main():
    curDir = os.path.dirname(os.path.abspath(__file__))
    # cd.generate_card_deck_to_file(curDir + '/cardDeck.txt')
    cardDecks = cd.read_cards_from_file(curDir +'/cardDeck.txt')
    if(cardDecks != None):
        for cardDeck in cardDecks:
            if cardDeck is not None:
                game = cdg(cardDeck)
                print(game.calculate_answer())
if __name__ == "__main__":
    main()